# from graphviz import Graph
import File

class ArbreBinaire():
    def __init__(self,elt,fg=None,fd=None):
        self.racine = elt
        self.filsGauche = fg
        self.filsDroit = fd

    def estFeuille(self):
        return self.filsGauche == None and self.filsDroit == None

    def affiche(self,showVide = False):
        a = Graph(graph_attr={'rankdir':'TB'})
        a.node('r',str(self.racine))
        def aff(arbre,graphe,nom):
            if not arbre.estFeuille():
                if arbre.filsGauche != None:
                    graphe.node(nom+'0',str(arbre.filsGauche.racine))
                    graphe.edge(nom,nom+'0')
                    aff(arbre.filsGauche,graphe,nom+'0')
                elif showVide:
                    graphe.node(nom+'0','',shape="none")
                    graphe.edge(nom,nom+'0',arrowhead='tee',dir='forward')
                if arbre.filsDroit != None:
                    graphe.node(nom+'1',str(arbre.filsDroit.racine))
                    graphe.edge(nom,nom+'1')
                    aff(arbre.filsDroit,graphe,nom+'1')
                elif showVide:
                    graphe.node(nom+'1','',shape="none")
                    graphe.edge(nom,nom+'1',arrowhead='tee',dir='forward')
        aff(self,a,'r')
        # print(a.source)
        a.view()

def sortiePlusProche(a):
    """
    Fonction permettant de trouver le chemin menant à la plus proche sortie du labyrinthe.
    Entrée : a -> arbre binaire réprésentant le labyrinthe (voir code ci-dessus)
    Sortie : au choix, soit une chaine de caractère expliquant le chemin à suivre,
                       soit rien si on affiche le chemin à l'aide de print (plus complexe).
    """
    F = File.File()
    F.Inserer((a,'Début '))
    while not F.FileVide():
        arbre,chemin = F.Extraire()
        if arbre.estFeuille():
            if arbre.racine == "Sortie":
                return chemin+" Sortie !"
        if arbre.filsGauche != None:
            F.Inserer((arbre.filsGauche,chemin+"gauche "))
        if arbre.filsDroit != None:
            F.Inserer((arbre.filsDroit,chemin+"droite "))

laby = ArbreBinaire((2,4))
laby.filsGauche = ArbreBinaire((2,3))
laby.filsGauche.filsGauche = ArbreBinaire((2,2))
laby.filsGauche.filsGauche.filsGauche = ArbreBinaire((0,2))
laby.filsGauche.filsGauche.filsDroit = ArbreBinaire((3,1))
laby.filsGauche.filsGauche.filsDroit.filsGauche = ArbreBinaire((2,1))
laby.filsGauche.filsGauche.filsDroit.filsGauche.filsGauche = ArbreBinaire((0,1))
laby.filsGauche.filsGauche.filsDroit.filsGauche.filsDroit = ArbreBinaire("Sortie")
laby.filsGauche.filsGauche.filsDroit.filsDroit = ArbreBinaire((4,1))
laby.filsGauche.filsDroit = ArbreBinaire((3,3))
laby.filsDroit = ArbreBinaire((5,2))
laby.filsDroit.filsGauche = ArbreBinaire((5,0))
laby.filsDroit.filsDroit = ArbreBinaire((4,5))