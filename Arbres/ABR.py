# from graphviz import Graph

class ABR():
    """ Classe implémentant un Arbre Binaire de Recherche """
    def __init__(self,r,fg=None,fd=None):
        """ Constructeur.
        Entrées : racine (elt) x filsGauche (ABR) x filsDroit (ABR)
                  Par défaut les fils sont mis à None.
        Sortie : un ABR
        >>> ABR(4)
        <ABR.ABR object at 0x...>
        >>> ABR(4,ABR(3),ABR(2))
        <ABR.ABR object at 0x...>
        """
        self.racine = r
        self.filsGauche = fg
        self.filsDroit = fd

    def estFeuille(self):
        """ Renvoie True si l'arbre est une feuille, False sinon. """
        return self.filsGauche == None and self.filsDroit == None

    def Inserer(self,elt):
        """ Permet d'insérer un élément au bon endroit dans un ABR.
        Entrée : elt -> l'élément à insérer.
        """
        if elt < self.racine:
            if self.filsGauche == None:
                self.filsGauche = ABR(elt)
            else:
                self.filsGauche.Inserer(elt)
        else:
            if self.filsDroit == None:
                self.filsDroit = ABR(elt)
            else:
                self.filsDroit.Inserer(elt)

    def Rechercher(self,elt):
        """ Permet de rechercher un élément dans un ABR.
        Entrée : elt -> l'élément recherché.
        Sortie : True si l'élément est trouvé, False sinon.
        """
        if self.racine == elt:
            return True
        elif elt < self.racine:
            if self.filsGauche == None:
                return False
            else:
                return self.filsGauche.Rechercher(elt)
        else:
            if self.filsDroit == None:
                return False
            else:
                return self.filsDroit.Rechercher(elt)

    def affiche(self,showVide = True):
        """ Permet d'afficher et d'exporter un ABR sous forme de diagramme au format pdf. Nécessite la bibliothèque graphviz. """
        a = Graph(graph_attr={'rankdir':'TB'})
        a.node('r',str(self.racine))
        def aff(arbre,graphe,nom):
            if not arbre.estFeuille():
                if arbre.filsGauche != None:
                    graphe.node(nom+'0',str(arbre.filsGauche.racine))
                    graphe.edge(nom,nom+'0')
                    aff(arbre.filsGauche,graphe,nom+'0')
                elif showVide:
                    graphe.node(nom+'0','',shape="none")
                    graphe.edge(nom,nom+'0',arrowhead='tee',dir='forward')
                if arbre.filsDroit != None:
                    graphe.node(nom+'1',str(arbre.filsDroit.racine))
                    graphe.edge(nom,nom+'1')
                    aff(arbre.filsDroit,graphe,nom+'1')
                elif showVide:
                    graphe.node(nom+'1','',shape="none")
                    graphe.edge(nom,nom+'1',arrowhead='tee',dir='forward')
        aff(self,a,'r')
        print(a.source)
        a.view()
